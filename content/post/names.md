---
date: 2016-10-07T00:46:44+02:00
description: "A list of common names and titles of the inhabitants of the planes."
draft: true
featured: false
image: ""
share: false
slug: common-names
tags:
- names
- meta
title: Common Names
---

# Auja

Bakai

Horow

Wralekai

**Calos Rane** - character

**Ivikhar** - place

Naetic

**Yezhul** - Auja surname

Rumura

Bedira

Luris

Wrauvris

Xevash

Atixaura

Mersoth

Quinarth

Jevith

Medolth

Taqua (ta-quay)

**Mentemheb** - Auja name

**Amenemhet** - auja name

**Aahepthes** - auja name

Bekilah

*Ebonutaraa*

**Nahaminah** - auja name

Nanekhu

Tatekhtou

Bakenoteph

**Taheres** - auja name

Menkaphres

**Herhet’su** - auja name

## Sub clans

- Amentuankh

- Anhurnekht

- Hatukhnu’mamen

- Sa’inubeu

- Ur Atum

- Atef

- Petiri

- Aau

- Sebek-em-of

- Aiemapt

- Hapentmat

- Nekhbet

- Ameniritis

- Panya

- Apui

- Shai

- Mosegi

- Takarut

- Abuskhau

- **Uah-pahret** - auja term

- Apa

- Odion

- Nu

- Sent-en-antef

- Taurt

- Iseueri

- Nefer-ra

- Mandisa

- Thot-hartais

- OJufemi

- Miane

- Hon-to'neb

- ​Ihena'cho

  ​

# Others

Figchen

Aube / Obe

Sindo

Tymoshenko

Batavia

Yeji

Unu

Suet

Karambit

Falx

Desis

**Raexetheus**

Zedeidon

**Rumios**

**Niron**

**Xuvanes**

**Phykterus**

Zulius

Xentytion

Nezias

Taelor

Asan

Cayne

Leviye

Camden

Laenard

Barreth

Fransix

Fynnegun

Brunnosname

Staggair

Cuamdeq



# Behemoths

**Vyg'dug**

**Urdi**

**Grum'guak**

**Gribraez**

**Traatheelydh**

**Kauldurek**

Staq'zoliz

**Rugguarak**

# Plane dwellers

Nelazar Deathbloom

Zrozis Siphon

**Hulos** The Eradicator

**Likhar** The Corrupted

Todulus The Harvester

**Staukhar** The Living

Cheixor The Abomination

Stolak The Analyzer

Chrezhul Blackhand

Tazhul The Analyzer



# Creatures



Ekrefs

Chru

Fled

**Pruh**

**Blaist**

**Edimnirs**

Chiplurx

**Niglyrk**

**Awuttain**

Zeodlufs

Oiqi

Gaf

Eagyp

Qeas

Ugofrairn

Wyfoph

Xondeolk

Chlolmolm

Chrocux

Mirx

Blairs

Ford

NafsNam

Chobdims

Chemdars

**Nyshtyh** - auja creature name

**Sedrycs**

Uxymmal



# Naethryn

Nuru Rahal

Kamuzu Kanaan

Gaber

Reda Qureshi

Kamuzu 

Radames Srour

Ishaq Gerges

Nader Boutros

Mazen **Dagher**

Bassam Koury

Nubia Naser

Fatma Mubarak

Kiara Awad

Sally Naifeh

Talibah Guirguis

Gameela Baz

Galela Maalouf

Selki Bata

Hafsah Nazari

Raneem **Toma**

Amr Seif

Hamadi Saliba

Hosni Said

Kareem Salib

Sadiki Bata

Osaze Assaf

Selim Safar

Osaze Bazzi

Momo Shamoun

Selim Koury