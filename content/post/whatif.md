---
date: 2016-10-07T01:05:23+02:00
description: ""
draft: true
featured: false
image: ""
share: false
slug: what-if
tags:
- Artefact
title: What if?
---

What if your whole world was ripped apart? What if you had no idea what you have become after the break?
What if the world is built upon a chaotic and
What if the very structure of reality could be torn and rent and moulded by an unknown source of power?
What if you could use it to tear a hole into a power source and drain it’s essence to power massive cities?
What if the only way to open a rift is through summoning essence into someone?