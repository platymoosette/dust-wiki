---
date: 2016-10-07T00:53:58+02:00
description: "Possible character sheet"
draft: true
featured: false
image: ""
share: false
slug: test-char-sheet
tags:
- test
title: Test Character sheet
---

# In-depth Character Sheet

                                                                                       

             

"Quote"

- Book, Chapter

## General Characteristics

Name:

Race:

Other Names:

Titles:

Appearance:

Birth Date:

Birth Place:

Current Residence:

Alternate Forms:

  (What can your character turn into, voluntarily or involuntarily, while retaining control over that form or not?)

Abilities:



### Personal Characteristics

Primary Objective:

(What is your character's main goal? This should be the thing that drives their part of the plot.)

Secondary Objectives:

(What other goals does your character intend to achieve? These should drive subplots.)

Desires:

Secrets:

(What does your character know that must not be told to anyone?)

Quirks:

(What about your character makes them different from everyone else?)



###      Mental Characteristics

Known Languages:

Lures:

(What is your character drawn to?)

Savvies:

(What are they very familiar with and/or very good at doing?)

Ineptities:

(What are they simply unable to understand?)

Temperament:

(Is your character choleric, sanguine, melancholic, phlegmatic, phlegmatic II, or supine/leukine?)

Hobbies:

(What activity or interest doe your character pursue simply for pleasure?)

### Intellectual Characteristics

Logical-Mathematical:

(How well can your character use reason and logic in their thinking?)

Spatial:

(How well can your character create an image in their mind?)

Linguistic:

(How good is your character with words, written and spoken?

Bodily-Kinesthetic:

(How well does your character control their body motions, how well do they handle objects? How clear is their sense of goal of physical action?)

Musical:

(How clear is your character's perception to sounds, music, tones, and rhythms?)

Interpersonal:

(How well does your character interact with and understand others?)

Intrapersonal:

(How well does your character understand their self?)

Naturalistic:

(How well does your character understand their natural surroundings?)

Existential:

(How well can your character understand phenomena or questions beyond sensory data?)

###      Philosophical Characteristics

Morality:

(What divides your character's definition of good and bad)

Perception:

(How do they perceive their situations? Optimistically? Pessimistically? Sadistically? Masochistically? Ideally? Realistically? You might answer by having them tell whether 'the glass is half empty or half full', and each of these perceptions should give a different answer.)

###      Spiritual Characteristics

Religion:

(What religion does your character identify with? Christianity? Atheism? Pastafarianism?)

Superstitions:

(What superstitions does your character subscribe to? Throw salt over your shoulder to relieve bad juju? Your ancestors are watching over you? Dancing like a monkey gives you monkey-like powers?)

Virtues:

(Does your character exhibit Chastity, Charity, Temperance, Diligence, Humility, Kindness, Patience, or Justice? Only the 8 listed should be used, but more than one can be used.)

Vices:

(Does your character exhibit Lust, Greed, Gluttony, Sloth, Pride, Envy, Wrath, or Despair? Only the 8 listed should be used, and only the ones whose opposites are not listed above should be used.)

###      Supernatural Characteristics

Ability:

(What is the name of your character's power? Sonic Scream? Reactive Adaptation? Toxic Flatulence?)

Element:

(What element does your character control or is classified under? Fire? Light? Surprise?)

Strengths:

Weaknesses:

Restrictions:

(What can't they do with their power or what balances it? They can only use it when they genuinely need it? They can't use it for more then five minutes without serious side-effects? It gives them intense gas and no one wants to be around them?)

###      Likes and Dislikes

Likes:

(What kinds of things does your character like? Dogs? Pizza? Alfred Hitchcock?)

Dislikes:

(What kinds of things does your character no like? People? Burgers? Angry Birds?)

###      Apparel

Equipment:

(What equipment does your character carry with them? A gourd? A toolbox? A bag of hair?)

Wardrobe:

(What kind of clothing does your character have? Mostly jocky? Half goth and half average? Nerdy?)

###      Social Characteristics

Emotional Stability:

(How much emotional inflictions can your character take before they break? Lots? None at all? Negative amounts?)

Humor:

(What does your character think is funny? Blond jokes? Death? Farting?)

Reputation:

(How is your character viewed by their peers? Are they held in high esteem? Do they think he's pure stupid? Do they think he's an alien?)

Status:

(What is their social status? Quiet? Popular? Dead?)

###      School and Work

Degrees:

(What college degrees does your character have? Bachelor's? Doctorate? None?)

Education:

(How much education has your character received? Up to middle school? College graduate? Everything from elementary school to martial arts of every kind?)

School:

(What elementary, middle, high schools, and colleges and universities did your character go? Manzanita Elementary and North Valley High? Jerry Springer Private School? None?)

Study Habits:

(How often does your character study and how thoroughly? Every free hour? Not at all? They have someone else study for them somehow?)

Learning Type:

(Is your character a Auditory, Visual, Kinesthetic learner?)

Occupation:

(What does you character do for a living? Graphic design? Write books? Star as the buttmonkey in a kid's show?)

Boss:

(What person or people is your character taking orders from or working for? Bill Gates? Admiral Hood? Mr. Pickles?)

Work Schedule:

Income:

(How much does your character make per hour and per year? $12/hour and $24000/year? $8/hour and $14000 a year? $75/hour and $52500/year?)

###      Interpersonal Connections

Immediate Family:

(Who are the relatives your character sees everyday or saw everyday growing up? Parents? Siblings? Children? Adopted parents?)

Close Relatives:

(Who are the relatives of your character that are only separated by one degree of blood difference? Uncles? Grandparents? Grandchildren?)

Distant Relatives:

(Who are the people your character is related to, but not at all closely? Grandparents-in-law? Great granduncle? Twenty-seventh cousin, 88 times removed?)

Ancestors:

(Who are some important ancestors in your character's family? Their great great granduncle? Them in a past life? Alexander the Great?)

Allies:

(Who or what company does your character and/or their company work with or fight alongside? Edgar Wicklow? McDonald's? The mafia?)

Enemies:

(Who is it that your character is always fighting or that they have set out to destroy? The Man in Black? Wally Wickenshield? The entire naked mole rat species?)

Followers:

(Who follows your character and is inspired by them or tries to copy them? That one annoying kid? Their friends? Their enemies?)

Friends:

(Who does your character feel safe with and could trust in a time of need? Barry Barrington? Veridian City? The Laz Group?)

Heroes:

(Who does your character look up to? Superman? Firemen? Hobos?)

Pets:

(What animals (or in some cases, people) does your character keep under their rule? A dog? A ferret? An alien?)

Rivals:

(Who is your character on good terms with, but is constantly in direct competition with? Their best friend? Their bigger brother? The smart kid in class?)

###      Physical Characteristics

Height:

in ( cm)

Weight:

lbs ( kg)

Nationality/Species:

Skin/Fur Color:

Hair Color:

Hair Length:

Eye Color:

Tail Length:

Tail Color:

Scars:

Tattoos and Piercings:

Locomotion:

(Plantigrade, digitigrade, or unguligrade)

###      Health and Fitness

Addictions:

Handicaps:

###      Sexual Characteristics

Gender:

Orientation:

Significant Other:

###      Story Information

Archetypes:

Tropes and Clichés:

Role:

Significance:

###      Personality



###      Development

Personal:

Social:

Physical:

Spiritual:

###      Biography

Infancy:

(What was you character's life like from age 0 to age 3?)

Childhood:

(What was your character's life like from age 4 to age 12?)

Adolescence:

(What was your character's life like from age 13 to age 19?)

Adulthood:

(What was your character's life like from age 20 to age 54?)

Seniority:

(What was your character's life like from age 55 until death?)

###      Other Information

           

                   