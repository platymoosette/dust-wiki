---
date: 2016-10-07T01:06:17+02:00
description: ""
draft: true
featured: false
image: ""
share: false
slug: artefact-name
tags:
- Artefact
title: ideas
---

# one or two

Geo Tagging - the Dust is an expansive plane of unfathomable distance. Much of it is reclaimed land. Those that are lucky cary geo taggers, that help pin point a way back through the reality distortions.



Gods - Not people. Not ‘beings’. Sometimes they are nothing but a congregation of feelings, a whisper through the naethyr. Other times they are overwhelming, terrifying in presence and power. Good, evil, solid, gas… Aethryn, Naethryn…




The kings strange friend:
To know the people. You needn’t live the life of your people to understand them. They are the best judge of their needs. The consult of those that live the life you are creating for them…



Bent belief: 
Just because there are those that twist and sully the work of the God, does not negate it’s existence or it’s rules. Just because the words of people can be false, does not mean the Gods are false. Knowing who speaks truthfully is the harder part of a belief that leaves men to rule it.



Have someone speaking over the radio. One person who figured it out… Only a few have old comm units, let alone working ones. Voice becomes a beacon and motivator/informant?

##Genna

Woken by pain! She was awoken by the suffering, then fed on it and made it worse. Stronger. Their deaths, their rotting, fueled her to the point of nauseating power. When they find her she freaks out, kills, runs, kills more, gets stronger, more feared..?
~~



## Weird beginnings..

Friendship in a glacier cave.

First impressions in the swamp.

Cleaning a meteor crater

Children in forbidden ruins.

Madness in another country

Language barrier in a glacier cave

Homeless with a witch

Lost at sea with a half-orc

Clones of a nomad

New year's eve with a Spartan

Jungle exploring with an old enemy

Accused by a living snowman

Horror story of a horse

Climbing a volcano with a cop

Lost in the royal palace

Murder in a video game universe

Invitation to a mountain summit

Ruler of the airport

Failure in the theater

Apocalypse in the workplace

Embarrasment in another country

Frustration in a utopian world

Treasure of a storm cellar

Trust in an underground pit

Trust in winter

Long lost twin in a bank robbery

Animal cruelty in a future world

Assistant in the local university

Super villain in another dimension