---
date: 2016-10-08T19:33:05+02:00
description: "One of the first of the Auja to tap the Naethyr, she was instrumental in discovering it's uses. In the tale of the Behemoth."
draft: false
featured: false
image: "/images/nahaminah.jpg"
share: false
slug: nahaminah
tags:
- Character
- Aujauran Se’ra
- Nahaminah
- Hul'hetsu
title: Nahaminah gerz'hul Yarra


---

# General info

Race: Aujauran Se'ra

Gender: Female

Mutation: [Hul'garrei](/artefact/hulgarrei)

Birth place: Aujauran

Relations:

- Brood Cousin: [Amenemhet yarz'hep Yarra](/character/amenemhet)
- Mother: Yarra Nan'hep Atef
- Brood mate: [Aahepthes gerz'hul Teheres](/character/aahepthes)

Abilities:

Appearance:

## Bio

Born into the Auja during the 

strong bond with aahep

---

## Personal Attributes

**Primary Motivator: **Achievement, to succeed, overcome obstacles, be the best.

**Emotional Disposition: **Melancholy

**Moodiness: **Quick tempered

**Outlook: **Idealistic

**Integrity: **Industrious

**Impulsiveness: **Rash, spontaneous

**Boldness: **Intrepid

**Agreeableness: **Empathic

**Interactivity: **Engaging.

**Conformity: **Outlier. Conforms long enough to get what she wants.

### Personality

**Humour: **None.

**Topics of Convo: **Art, creation.

**Adherence:**

**Tolerance of others:**

**Quirks and Habits:** No humour. Likes to make a scene, exhibitionist? 

**Hobbies: **Fortune telling? 

**Religion: **Brought up after the devestation, with nothing to believe in. Her and Aahep had to find a god to believe in. She fell in awe of the Naethyr, it's worship turned into her saviour. And hope.

------

**Surface affectations and personality**:

**Physical affectations**:

**Inner demons and conflicts**:

**Personality**:

**Worldview:**

**Goals and motivations:**

**Anima:**

(How does your character act when they are really being their self?)

**Persona:**

(How does your character act to hide their real self?)

**Decisions, actions and behaviours**

---

# Character Arc:

---



# Links

