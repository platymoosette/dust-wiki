---
title: Aahepthes gerz'hul Taheres
date: 2016-10-08T19:31:50+02:00
slug: aahepthes
description: Elder of the Hul'hetsu and main antagonist in the tale of the Behemoth.
tags:
- Character
- Aahepthes
- Aujauran Se’ra
- Hul'garrei
draft: false
featured: false
image: "/images/aahep.jpg"
share: false





---

#General Info

Race: Aujauran Se'ra

Gender: Male

Mutation: [Hul'garrei](/artefact/hulgarrei)

Birth Place: Aujauran

Relations:

- Brood mate: [Nahaminah gerz'hul Yarra](/character/nahaminah)
- Mother: Taheres Shai'hul Apui

Abilities:

Appearance:

## Bio

Born in a time of upheaval, 

## Personal Attributes

**Primary Motivator**: Liberation. To free the Auja from needing the Behemoths protection. 

**Emotional Disposition:** Oscillates between calm and angry.

**Moodiness:** Phlegmatic

**Outlook**: Distrustful of others.

**Integrity: **Works towards his main goal, despite if it hurts people.

**Impulsiveness**: Focused

**Boldness:** Confident, Daring, rebellious 

**Agreeableness**: Intractable 

**Interactivity**: Evasive.

**Conformity:** Rebellious. Free thinking

### Personality

**Humour:**  Cynical and dry

**Topics of convo: ** Only talks to you if he needs something. Or to manipulate information.

**Quirks and habits: **Facial tic? Smelling things?

**Religion: T**he Naethyr is his religion. He has found the other plane, and he has worshipped it's existence. He is aiming to summon the most powerful naethryn into himself, to be able to take over the behemoth... Or to summon a naethryn into the behemoth. One that he's "working" with?

**Hobbies:** Hunting... 

------

**Surface Affectations: **Strong and angry from birth. He was born 

**Physical Affectations**:

**Personality**:

**Inner demons and conflicts**:

his soft spot was Na'minah? The one he couldn't say no to. They were brood mates. She was there for him through everything, competing with him, pushing him further than he would alone. 

But then the idiot Calos came along. And stole her heart. And ruined everything. He took Na'minah away from him and their goal. Leaving him alone and angry. He resents Amen for not stopping Na'minah sooner. 

**Worldview:**

**Goals and motivations:**

**Anima:**

**Persona:**

**Decisions, actions and behaviours:**

# Character Arc

------

# Links