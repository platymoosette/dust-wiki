---
date: 2016-10-13T19:12:52+02:00
description: "The controversial daughter of Nahaminah and main character in the tale of the Behemoth."
draft: false
featured: true
image: "/images/tej.jpg"
share: false
slug: tejheira
tags:
- Character
- Tejheira
- Aujauran Se'ra
- Ivikhar
- Naethyr
title: Tejheira Kilau'tep

---

# General info

Race: Aujauran Se'ra / Ivikhar

Gender: Female

Mutation:    

Other names: Tej, Kilau

Birth Place: Aujauran Se'ra

Relations:

- mother: [Nahaminah](/character/nahaminah)
- father: [Calos](/character/calosrane)
- tekem'sa: [Amenemhet](/character/amenemhet)

Abilities:

Appearance:



## Interesting Traits

- Some Auja develop hand like feet that allow them to walk on a variety of surfaces, including walls and ceilings. Gecko-like claws that either dig in and grip or suction on. 

---

## Personal Attributes

**Primary Motivator: ** _Belonging_

**Emotional Disposition: **curious. Attentive

**Moodiness: ** Vascilates between wild and over cautious.

**Outlook: **Wary. Unsure of the world.

**Integrity: **Manipulative. Truth is watery. No one tells her the truth.

**Impulsiveness: ** Flighty. 

**Boldness: **Cautious. But bold.

**Agreeableness: **Tense and wary. Rebellious3

**Interactivity: **Reserved. Aware how much people dont like interacting with her. Doesn't know what to believe, so believes nothing.

**Conformity: **Entirely heterodox 

##Personality

**Humour:** Surreal

**Topics of Conversation: **Her dreams. Her visions. Questions about the outside world. What it's like. What it looks like. What it was like to move around. Who the Auja were, who they are. 

**Religion: **Non-believer. She was not brought up with anything, as she was secluded from most knowledge on Auja religion and culture.

**Quirks, habits, oddities: **Humming. Though that was discouraged.

Day dreaming. Sleeping in odd places. 

**Hobbies & enjoments: **Reading. Though not allowed... Riddles, drawing.

---

**Surface Affectations and Personality**

Birdlike, her actions are sharp and staggered. Like she doesn't belong in her body. She is quiet, she likes to listen to the subtle calls inside the Behemoth. The humming. The one she hears in her head constantly, and tries to hum back herself. dreamy and imaginative. Prone to not answering questions, or answering with something else. 

She feels like a ghost. Lost in a nebulous world of little interaction or information. She is used to people not talking or responding to her. Her questions go unanswered. Her requests unheard. When someone does, she is surprised, and unprepared to respond with  her truth. Rather struggles to word things. Which is why she is misinterpreted or offensive without meaning to. 

She feels like an observer in a world she doesn't belong.

**Inner demons and conflicts: **Torn between hating and resenting what her mother chose to do to cause everyone to hate her and her child. And hating the people that perpetuate it without telling her why. Cares for Amen, as Amen cares for her. But there's no love. She can still see her sad and watchfull. Though she can't verbalise those feelings, she /knows/ them.

**Worldview: **The world is something she can't touch. The power and energy of Naethyr rippling through the air around her, through the people she lives with, through the items they use. But she's not allowed to use it. She is confused by the world, not understanding it's reasons or machinations. Or where she fits in it. She can feel the tug and pull of the powers flowing around her, of the behemoth and its symbiotic humming. Of every scrap of energy used for glyphing and calling.

**Goals and motivations: **To learn who her mother was and what she did. And who she is herself. Why she is isolated from everyone and everything in the culture shes surrounded with. To find out more about the people, the place, the reasons why she is treated the way she is.

**Anima:** 

(How does your character act when they are really being their self?)

**Persona:**

(How does your character act to hide their real self?)

#### Decisions, actions and behaviours

**Character Arc**: 

---



##### Links

