---
date: 2016-10-08T19:34:16+02:00
description: ""
draft: false
featured: false
image: ""
share: false
slug: ogtbish
tags:
- Character
- Ogtbish
- Behemoth
- Aujauran Se'ra
title: Ogtbish Bashimur


---

# General info

Race:

Gender:

Titles:

Other names:

Appearance:

Abilities:

## Bio



---



## Personal Attributes

**Primary Motivator**: 

**Emotional Disposition:** 

**Moodiness:**

**Outlook**: 

**Integrity**: 

**Impulsiveness**: 

**Boldness:** 

**Agreeableness**:  

**Interactivity**: 

**Conformity:** 

**Humour:**  

**Topics of convo**: 

**Quirks and habits**:

**Religion**: 

**Hobbies:**  

---

### Surface Affectations and Personality

**Physical affectations**:

**Anima:**

(How does your character act when they are really being their self?)

**Persona:**

(How does your character act to hide their real self?)

**Personality**:

**Inner demons and conflicts:**

**Worldview:**

**Goals and motivations:**

#### Decisions, actions and behaviours

**Character Arc**: 

---



##### Links

