---
date: 2016-10-08T19:33:25+02:00
description: ""
draft: false
featured: false
image: "/images/calos.jpg"
share: false
slug: calos-rane
tags:
- Character
- Calos
- Ivikhar
- Ogtbish
title: Calos Rane


---

# General Info

Race: Ivikhar

Gender: Male

Titles: 

Other names: 

Appearance: 

Abilities: 

## Bio

The Traveling Fool from Ivikhar, which is technically in the middle of nowhere, but because of the places around it, it became a hub for travellers. 

Born and raised to be as useful as possible, as soon as possible, in a tiny settlement in the ruin of an ancient city. 



Hates the Auja. Wants to go. Home.

Anywhere else is also accepted. Anywhere. Else. But in the middle of empty planes.

It's hard to not get angry, when you're stranded in the middle of the Planes, and the only sanctuary is in a mess of decaying, \~dust warped  ruins. Of a very complicated people. 

# Personal Attributes

**Primary Motivator: **  Acquisition. 

Getting off this fucking dead rock. Chaos. To disrupt, cause discord, 

**Emotional Disposition: ** Angry

**Moodiness: ** Prone to fits of crazy rage. Moments of recollection, talking to people in times long past. Jumping to weird conclusions, sure of the universe's conspiracy to screw him over.

**Outlook: ** Distrustful. Hates the Auja, think's they're awful, beastial, nomadic Abhorrent . They meddle with the dark oceans of naethyr, and dragged him into it. 

**Integrity: **

**Impulsiveness: **

**Boldness: **

**Agreeableness: **

**Interactivity: **

**Conformity: **

**Humour: **

**Topics of Convo: **

**Adherence:**

**Tolerance of others:**

**Quirks and Habits:** 

**Hobbies: ** 

**Religion: **

------

### Surface affectations and personality

**Physical affectations**:

**Persona**:

**Anima:**

**Personality**: 

**Inner demons and conflicts:** Selfish, deceitful

**Worldview:**

**Goals and motivations:**

#### Decisions, actions and behaviours

##### Character Arc:

------



# Links

