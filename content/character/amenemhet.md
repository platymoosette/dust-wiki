---
date: 2016-10-08T19:32:44+02:00
description: "A kind and devoted Auja in the midst of rebellion and revolt. She is part of the Tale of the Behemoth."
draft: false
featured: false
image: "/images/amen.jpg"
share: false
slug: amenemhet
tags:
- Character
- Amenemhet
- Aujauran Se'ra
- Abusk'hep
title: "Amenemhet yarz'hep Yarra~"


---

# General info

Race: Aujauran Se'ra

Gender: Female

Mutation: [Abusk'hep](/artefact/the-abuskhep) 

Birth Place: Aujauran

Relations

- Brood Cousin: [Nahaminah gerz'hul Yarra](/character/nahaminah)


- Mother: Yarra Nan'hep Atef

---

# Bio



------

# Personal Attributes

**Primary Motivator: **Service, to her behemoth, committed to her Hon-to'neb.

**Emotional Disposition: **Calm

**Moodiness: **Even tempered.

**Outlook: **Foreboding? Fears the worst

**Integrity: **Pragmatic

**Impulsiveness: **Deliberate, thoughtful.

**Boldness: **Tentative until pushed.

**Agreeableness: **Altruistic

**Interactivity: **Cryptic

**Conformity: **Traditional. Tows the line.

------

## Personality

**Humour: **Dry

**Topics of conversation: **Religion. End of the days. The past.

**Quirks and habits: **Superstitious. Plays with her fingers when nervous (from drawing warding and prayer glyphs in the air). Loves writing, collecting letters. But her glyphs don't work the same without the Behemoth.

But she hates stories of the past, it upsets her, she knows they will get you hurt or killed.... 

**Hobbies: **Carving. Into whatever she can. She misses it from working with the Behemoth. 

**Religion: **After the devastation, she saw how the [Class: Hul'hetsu](/Class_ Hul'hetsu.md) saved and protected them, and became a strong believer, follower and supporter. Fell in wholeheartedly. But time showed the grittier side of their rituals and choices...

**Adherence: **Strong. Intolerant of others in theory, but in her heart she is not so sure.

---

**Inner demons and conflicts**:

Torn between every opposite. Sense of idealism and "morality" (relative), often engaging in personal rescue efforts and mediating difficult situations, she's also well aware of brutal facts and realities of life. Of how things go wrong. It ends up leaving her paralyzed. Horrified at how ineffective her actions are, waiting to do the right thing, but how do you know what or who is right? 

Conflicted, caring, suffering saint.

**Surface Affectations: **

**Physical affectations**:

**Worldview:**

**Goals and motivations:**

**Anima:**

(How does your character act when they are really being their self?)

**Persona**:

(How does your character act to hide their real self?)

**Decisions, actions and behaviours**:

#Character Arc:

She realises that the Hul'garrei using the Naethyr in the beginning was what caused the Behemoth to shut down. And they didn't actually "save" them. They just made the situation worse. 

Realises that what they're eating is the foul flesh of the denoth summoned from the other Plane, and what is causing illness to the Auja and ogtbish, while the Hul'garrei grow stronger and less Auja the more they summon. But without it, they would die of starvation... So she stoically keeps her mouth shut.

---

# Links