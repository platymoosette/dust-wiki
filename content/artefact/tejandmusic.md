---
date: 2015-10-14T19:39:50+02:00
description: "How Tej hums to communicate"
draft: false
featured: false
image: ""
share: false
slug: she-hums
tags:
- Artefact
- Story Arc
- Ogtbish
- Aujauran Se'ra
- Tejheira
title: Ogtbish and the Aujauran

---

**She hums**

Originally they had those that spoke to ogtbish through sound, through music, the glyphs sounding together to create songs.

They echoed through his chambers, his very being.

Once he shut down, they stopped that. Thats why they only write glyphs now, no one is taught how they sound.

Except Tej *knows*. She was born humming. Surrounded by glyphs upon life, upon breathing again, she was in tune and thrumming with their sound. She doesn't know their shape, doesn't know what they mean or how to form them. She calls upon them just from having heard or felt them before, from feeling it around her with the Auja constantly. *And the rest of the symbiotes calling to her, whispering and crying through his arterial chambers, hinting at other songs and sounds and places.*

That's also what disturbs the Auja so greatly. Both that she do that without having it taught, and to hear the glyphs again not in their music, but through the unsettling humming of an abomination.

---

Aamen was forced to never sing to him again. So she focuses on writing … all the glyphs she can. Her hidden project is a cave only filled with her story to ogtbish. The story of her people, of what she believed, what she saw and felt and understood. Hoping that if he cannot hear her, he could read her glyphs.

She is torn with Tej. Hearing her hum rips a hole in her composure every time. A longing for the sound it resonates within her, a horror at having left it behind and the reasons why, and a cold fascination at what type of creature her cousin created. 