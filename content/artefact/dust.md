---
date: 2016-10-07T01:07:05+02:00
description: "The very matter of "
draft: false
featured: false
image: ""
share: false
slug: the-dust
tags:
- Artefact
- Dust
- Aujauran Se'ra
title: the Dust

---

The dust is Decay. The slow warp and rot of distortion. Eating away at structure and reality. Festering with power and entropy. A plague. No one is immune to it’s effects. But the effects and changes can differ.
It is decaying them. Ill, ravaged, twisted, mutated. Some live, changed by the toll of Dust. Others die. Slowly degrading. Some do manage to adapt and thrive, finding pockets of safety. 
But on the whole: Destructive.
The clever and lucky ones found protection from the Dust.
The City [Vars’in](/loci/varsin-city) is one of the few locations able to power it’s protective barriers with the Dust itself. Running on it but creating an area void of it’s effects. Though it was built to run on Dust, it was before it had altered and mutated. So while still giving power/energy it is wrong, damaging the City in tiny ways. Constantly distorting ever so slightly how the City runs and awakens. Things are not as it remembered. It is not as it remembered. The world is clearly off, but to find out why……



The Dust is not just destructive in itself, but entropy is rife in it’s presence…
There were many plagues . That did many awful things and killed the majority of those with it. Others slowly adapted, or just lived with the consequences.
Slow decay. Mutation. Shortened life, bleeding, organ failure, break down, limb loss, etc…