---
date: 2016-10-07T00:56:34+02:00
description: "A race of nomadic people that settled into a symbiotic relationship with a Behemoth."
draft: true
featured: false
image: ""
share: false
slug: the-auja
tags:
- Artefact
- Aujauran Se’ra
- Race
title: "the Aujauran Se'ra"
---

# Who are they?

A Race of people that were connected and symbiotic with the being they lived on, a Behemoth called Ogtbish Bashimur. They could mould, alter and build into/with the very land, ground and rock of Ogtbish, connected and working together. The elements were always a challenge, but Ogtbish Bashimur could feel the ripples of waul, sense the currents of warping Naethyr, and in turn had his own deterrant, repelling the twisting corrosion. Protected them from the brunt. They still had to contest with small infiltrations, trails and remnant whisps that still snuck through his protection. Infact, this was part of the benefit to Ogt, to make sure what got through didn't burrow deeper and infect his more vulnerable symbiotes.
(why them summoning naethyrn into him later was an insult...)

Ogtbish kept them safe and kept them moving, able to navigate the planes like they never could alone. Without it they are landlocked and stranded. 

Now, Dust has corroded and eaten away at their city dwelling up top, forcing them to move deeper into the cavernous under belly of the silent behemoth. ..

# 5 traits / things that stand out:

they have a very long adolescent stage, taking them much longer to mature. The Behemoth was part of their cycle, helping instigate the hormonal change with the bursts of aethyr running through it. Without it, they have struggled to mature their children, meaning breeding ages have dwindled, especially with so many dead from starvation and malnutrition. Population is under major threat

# Physical Description:

Skin a cool terracotta, matching the rock of the Behemoth. Skin dry and weathered by living in the scouring wind. Heavy lidded eyes, adapted to the glare of the Sky, they struggle in the dark of underground.

## Background

 Life is brutal, life is difficult. The elements are constantly shifting, unbearable without shelter. Little grows in the ground around them. Predators, desperate and starving themselves, are a constant threat. They lived in Symbiosis with their behemoth, worked with it’s flesh like rock and stone, like clay. But no longer… Now the ghosts of their efforts lie abandoned on the surface. Untouchable. The underground is rudimentary in design and construction, almost like access tunnels and storeage rooms. They’ve picked away in places and used discarded material to attempt rebuilding, but without cooperation from the host their work is hard and unsuccessful, no longer malleable to their touch. Just, useless rock.
With their God and life source having abandoned them, they are left confused and superstitious. Some try to repent and sacrifice their lives to rituals and offerings. Others turned to praying to Gods of others, Gods of the past, of the world, out of desperation. Others took it into their own hands. To be powerful enough to awaken or call back their Behemoth, or to control it…. Or to call another in it’s place…

**Internal Conflicts:**
**External Conflicts:**



# Story Notes:

 They half kept Tej because she was one of the few children. Even though not chosen to be conceived.  Without B shielding them, they were ravaged with infertility from the dust damage, as well as low resources. There are not many young, and they must be planned and accepted before conceived because of prioritising resources. The adults had to figure out how to survive longer. Defy the decay of the dust…
Without proper nourishment, the help of the B’s ecosystem removed, they struggle to not only conceive but keep their young alive. The first few years are the most fragile and fraught with illness. Ailments are rife, conditions harsh (some other fundamental difference? Maybe feeding on these pupae are doing something else to the young? Or the meat?)


/// They still have the remnants of nomadic life imprinted in their culture. They were used to travelling through different planes, to having constantly shifting resources, to have their safety and homes supplied to them, made for them, always there. They were a part of something larger.

## Things to cover:

- Family Structure.

  broods and tekem'sa. non-tradtional parents and reproductive cycles.

- Their environment. In dust ravaged surroundings. Their behemoth shutdown, disengaged from them, left them vulnerable to the elements and predators. They soon shut themselves inside for safety, leaving the upper cities to be eaten away and distorted by the storms. They don’t go above very often and avoided if possible. But they are ill equipped or adapted to survive underground, their eyes made for sun light glaring off the barrens.

.. Some people still cling to living up top, or to living near it,


- Resources.  Scarce in the middle of empty planes. Rudimentary systems inside still work, some “gardens” still remain. But nothing like before… And not enough to sustain all the inhabitants.

- Gender.  Almost like metamorphosis, different life stages occur that they don’t often achieve without the help of the Behemoth. (?) Did the summoning spur the changes instead? Either way, Summoning is a right of passage into another  phase. Maybe one they didn’t have before? Possibly two genders, with a third one barren?

The gender is more like a social or class difference. Those that can become guardian are the ultimate images of success. They have metamorphosised.


- Food!. Its a huge issue of survival. Few resources, nothing much grows in the dust around them. Occasionally wandering mutations or sprouting vegetation. But who knows if its safe?

They harvest the pupae of another symbiote race as food… They keep them in stasis, not growing, and either eat them or use their coccoon matter as food.

- The ritual of the feast. Able to supply a momentary bounty of meat. No one could/would question it other than a life saving miracle?

