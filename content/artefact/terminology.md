---
date: 2016-10-08T17:48:35+02:00
description: "Terms, words and colloquialisms gathered from across the Planes."
draft: false
featured: false
image: ""
share: false
slug: terminology
tags:
- Artefact
- Terminology
- Meta
title: Terminology

---

# People

- Nuru - the nomadic people to find the hibernating city of Var'sin and awaken it.
- Aujauran Se'ra - one of the Symbiotes on the Behemoth, Ogtbish. Their origin is lost to the Dust, a ravaged people who found refuge on a wandering Behemoth. 

# Powers

- Waul - the power of the Aethyr, energy before Dust distorted it. Now more likely to be Naethyr being pulled through from rifts like osmosing water.

- Aethyr - the magic before Chru changed, before the Dust was created, changed, corroded, distorted. With the presence of Dust it became unreliable.

# Gods, Beings and Otherlings

- Behemoths

# Places

- Chru - The world
- Var'sin - The City that was awoken

# Expressions

- Tak - tech

# Unexplainables

# Race Specific

## Nuru Language

- Satoh - Their name for magic?

## Aujauran Se'ra language

- Tekem'Sa - care givers, guides and teachers to auja children. 
- Brood - A group of children born at the same time and raised together, ignoring blood relations.
- Uah-pahret - Individuals refer to their class group as their Uah-pahret, but it carries a heavy sense of duty and responsibility too. It is their adult family, social structure and calling.
- Rioutha - the Feast 
- Daigo'shur - Behemoth's betrayal
- Wethyr - A silk like substance spun by the Auja, consisting of their secretions and that from the Naethyr beasts. It can be used to repell certain people, or repell anyone but certain people… 
- Denoth - beings of power, usually from other planes like the Naethyr, they can inhabit worlds and planes from leaking through rifts or from being summoned.
- Naethryn - beings of unpredictable, corrosive power.
- Aube - the power referred to by the Auja that they drain from the Naethyr




# Things

