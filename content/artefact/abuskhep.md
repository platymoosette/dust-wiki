---
date: 2016-10-07T01:25:50+02:00
description: "The Shapers of Ogtbish, a class mutation of the Auja"
draft: false
featured: false
image: ""
share: false
slug: the-abuskhep
tags:
- Artefact
- Class
- Aujauran Se'ra
- Abusk'hep
title: "The Abusk'hep"
---

# the shapers

Able to carve and shape the behemoth, or other materials. They know carving glyphs



# Links

[Amenemhet](/character/amenemhet)

