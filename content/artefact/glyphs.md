---
date: 2016-10-08T00:30:41+02:00
description: "Used by the Auja to communicate with their Behemoth, among other things."
draft: false
featured: false
image: ""
share: false
slug: glyphs
tags:
- Artefact
- Glyphs
- Aujauran Se'ra
title: Glyphs

---

[Ogtbish](/character/ogtbish) gave the [Auja](/artefact/the-auja) glyphs to use with him. They are like communications, system inputs, that make certain things happen.

some were for the [Abusk'hep](/artefact/the-abuskhep) to shape him, to carve to build to mould. 

Others were for the [Hul'garrei](/artefact/the-hulgarrei) to call upon certain spells, blessings? Armor, wards, to put around them, to ward off the [Creatures: Nyshtyh](/Creatures_ Nyshtyh.md) and [Dust Storms](/Dust Storms.md). They had to maintain the wards to keep safe, as they were also corroded by the dust with time. 

Glyphs also imbue items and Auja (not just Ogtbish) with certain attributes or protections. Not many offensive glyphs, more defensive and practical, like  heat protection, light reflection, cooling, changing smells, detecting dust creations, etc.

They are carved and/or painted, depending on if they are applied to Ogtbish, an object or skin.

Technically not a writing system, but after Daigo'shur it was used predominantly to talk to each other rather than Ogtbish, to tell stories and a form of history keeping by some still loyal to the Behemoth.



## Links

influence on glyph construction and use:

 [Ersu](https://en.wikipedia.org/wiki/Ersu_Shaba_script)

