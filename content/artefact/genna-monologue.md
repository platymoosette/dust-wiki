---
date: 2019-02-08T15:15:13+02:00
description: ""
draft: false
featured: false
image: ""
share: false
slug: genna-monologue
tags:
- Artefact
- Character
- Genna
- the Raging God
- Monologue
title: Genna's monologue
---

I was born to be a parasite. I was born to take your life, your hopes and your dreams, your future, your soul. drain them from you until your body deteroriates and slowly bleeds itself from existence. I will take it all and burn you dry, absorb you from the world. I will do this all and stil grow stronger.

I never asked to be this way, I never knew I was even like this until it took from me all I ever loved, all I ever knew, all I wanted to keep. Took it and destroyed it in front of my eyes. My parents thought I was god touched. But the touch of the gods is cruel and unthinking. Coursing power through your veins until you're nothing but a force of uncontrolled magic. Turning you into something, something you don't even understand. 

They thought I was a gift. But I wasn't. 

My mother was a healthy woman, bearing two children before me, content and happy in the tiny village nestled in the tree encrusted mountain her and my father called home. I was supposed to complete their little family, I was supposed to  be the last part in their lifes puzzle, not tear it apart. 

My conception began the slow decline into an agonising and difficult pregnancy for her, far beyond her previous experiences. Her life seemed to drain away before my fathers eyes. He could see me struggling against her taught, vein threaded belly near the end, while she sweated and shuddered through a half awake-half delirious state that forced her into months of bed rest, only just able to stay present enough to stay alive. He tended her every need, beyond distraught at the thought of both losing the woman that completed his soul and the precious gift they created before even holding it breathing in his arms. Keeping her comfortable as best he could was all he could do while she fought the raging battle inside her to keep me. His hands would trace the shape of me bulging from inside her, worry and fear seeping through the thin barrier of skin, permeating and mixing painfully with the soft burning pulses of my mothers failing heart. 

I knew them both before I entered this world, by their love and worry and fear throbbing through her life into mine, . I knew them both before they knew me, burning into the world.

I was borne on the flow of blood and pain, delivered into the trembling hands of my sobbing father. Clutched tight to his thundering thudding chest as he pressed his lips against my mothers sweat drenched forehead, his choking laugh of disbelief and relief washed over and cradled me in its trembling energy, while he stared at the drained yet radiant smile that was slowly drifting across her palid face. She was alive and breathing, and a little bundle of life was held gently between them. Despite the traumatic and difficult struggle I was cherished in that moment. Their triumphant and weak embrace blanketed me in the only moment of calm and happiness i can remember. 

She never did quite recover after that. Always easily tired and drained, her frame almost seemed to have shrunk into itself slightly, and her face retained the palour of the gravely pained. But she lived, and I was loved even more for it.

How i was aware of them and life before i even left the safety of her womb, i can't say. But my memories begin since I was created. 

I was aware of how much it strained them after I was born. Prone to bouts of depression and lethargy, they battled to keep me contained in their tiny cottage. I could sense the ebb and flow of people outside, the animals just outside my reach, calling me with their life force to touch them, hold them, connect them to mine. But my touch burned. From young they would hold me only through a layer of cloth, keeping their skin from mine even though I yearned for contact. They couldn't understand the bruises that blossomed on their skin when they held me, the sense of draining and nausea at my touch. I played with my siblings as much as I could, but after a few years the house hold seemed bleak and ill. My mother developed a stomach ache that nothing seemed to help, and soon she was eating so little I began to think she would disappear before my eyes. My father was struggling to keep the livestock alive so he could afford to feed and clothe us, but a plague was spreading through them. The ones that didn't slowly diminish into death became barren and their milk dried up forcing us to ask neighbors for help. But our house became known for its Ill effects, the town's people wary of lending Darem to our flocks when they weren't returned with foals or milk. 

Still I wasn't allowed outside. My parents said it was because I was too much for people to understand. They thought I had the blood of God's running through my veins, believing that was why I burned. But I was happy in my little world anyway. I helped my mother and brothers to tend the garden in hopes more would grow, I helped cook and clean and look after her when her waves of weakness came. She was always happy to have my embraces, even if through cloth. 

 