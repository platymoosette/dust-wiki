---
date: 2015-10-14T19:42:50+02:00
description: "The tale of the behemoth and it's symbiotes, the Aujauran Se'ra."
draft: false
featured: false
image: ""
share: false
slug: storyarc-auja
tags:
- Artefact
- Story Arc
- Ogtbish
- Aujauran Se'ra
title: Ogtbish and the Aujauran

---



# The Betrayal

In which Ogtbish feels betrayed by the Auja. That they would put his other symbiotes in danger, and himself, by poisoning his very being and matter. For disobeying him. So he settles down, sinking himself into the ground around him, shields sputtering out of existence and the familiar hum of his presence now a dead zone, a lack. 

Startled and 



# After the 

Their people have given up talking to their God, their beliefs rendered dead and useless. Now they worship and call upon another plane, an endless infinite of other beings, dark and made of Naethyr, able to twist and transform, rend, destroy, obliterate. To call on them and consume them, both their flesh and (n)essence. No longer do they feel the comfort of symbiosis and belonging to a greater being. Alive and free. Instead they must scuttle about inside the carcass of their God and hide, starving and lost. Calling upon the threads of something Dread, is their last chance at life. 



# People involved

w