---
date: 2016-11-19T19:51:09+02:00
description: "Physiology is weird"
draft: false
featured: false
image: ""
share: false
slug: scene-hands
tags:
- Artefact
- Aujauran Se'ra
- Tej'heira
- Scene
title: Hands

---



Tej watching Amen’s hands signal glyphs and noticing the differences

---

## who

[Amenemhet](/character/amenemhet) and [Tejheira](/character/tejheira)

## where

In the home shared between Amenemhet and Tejheira

## why

highlighting the differences between their hands. Differences between them.

Amen is fluid potent grace, sure of her motions and meaning, forming glyphs from chips and duggets of rock and viscous liquid. 

Tej is staggered across existence, time. Jagged and unsure, her movements staccato. Edgey and birdlike. Not belonging to the space

# what

Oh, how different she looked. It was not just her hands. Disturbing amount of fleshy, articulating fingers. Clearly unable to properly glyph. No, it was also her face. Her nose, small and round and *soft*.
Eyes sunken into dark questioning pools, nothing like their pale orbs. It was like looking into somewhere else, another world, or this one but just slightly *wrong*.
Disconcerting and slightly abhorrent, it takes some time getting used to, and even then staring too long feels unsettling. Her face felt fine to her... Later on, tired of their reactions, she would scare the younger brood on purpose, wiggling her fingers and making faces at them.



