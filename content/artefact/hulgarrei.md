---
date: 2016-10-08T15:40:55+02:00
description: "The Guardians of Ogtbish, they are a class mutation of the Aujauran Se'ra."
draft: false
featured: false
image: ""
share: false
slug: the-hulgarrei
tags:
- Artefact
- Class
- Hul'garrei
- Aujauran Se'ra
title: "the Hul'garrei"


---

# the Protectors



They used to be the ones in communication with Ogtbish, they spoke to him differently, had more of his perimeter senses, so they could help where things were being damaged by something.

 After Daigo'shur they became summoners.

### Relations:

— [Aahepthes](/character/aahepthes)

— [Nahaminah](/character/nahaminah)