---
date: 2016-10-02T18:31:13+02:00
description: "The different cultural expressions of the Aujauran Se'ra."
draft: false
featured: false
image: ""
share: false
slug: auja-culture
tags:
- culture
- meta
- names
- "Aujauran Se'ra"
title: "Aujauran Culture"
---

# Broods and family structures

Being a nomadic and resource aware race means they synchronise and plan their reproduction and breeding cycles. Children are born into broods and looked after by multiple people, depending on their class, or _Uah-Pahret_, more than lineage or relation to the child.

Parents are not always present in their childs upbringing, sometimes barely a part of their life at all. There can still be a strong connection between them, some times even a fond one, but generally ones duty and interest in the child dimishes greatly after birth and they return to their respective _Uah-pahret_. Parents are subsequently not the strongest relationship in an Auja's life. Broods are where most time is spent while growing up, forming sibling bonds and rivalries, although without much interaction between different broods.

_Tekem'Sa_ are the care givers and teachers. Auja that don't show strong symbiotic relations with the Behemoth will take on this role, working together as an insular group to look after each brood, a large undertaking. They teach culture, history, glyphs and their complex relationship with the Behemoth. They are more teachers and guides than tender or paternal figures.

---

## Reproduction

_Before Daigo'shur_

Auja can choose to reproduce. Two parents are not necessary, but in combination they can have different and/or heightened genetic expressions that are attuned to the Behemoth differently. Two people from the same _Uah-Pahret_ have a higher chance of a child having the same mutation and possibly stronger. Different but complimentary classes can increase the skill and strength of their childs mutation, sometimes allowing them a blend between two. Other times mutations just clash, creating mutes who cannot feel the connection to Ogtbish at all. They live a very isolated and different life in comparison to the rich canvas of the other Auja's symbiotic experience.

Individuals can choose to reproduce alone, and almost definitely know it will have their mutation, or ones in their family lines expressions. The children can sometimes be considered weaker, or not having a strong connection to the Behemoth, almost seeming dumb or autistic in some ways, and brilliant in others. It is, however, encouraged during times of struggle when the need for a specific _Uah-Pahret_ is needed, or when few partners are available, the risk of weakness becoming small and worth it.

Broods are made up of a variety of children, bound for different _Uah-pahrets_. 

_After Daigho'shur_ 

Classes or _Uah-Pahrets_ became irrelevant after Daigho'shur. With Ogtbish silent and absent, their connection to him via their mutations and the abilities that went with it were now obsolete. Disconnected. No longer a responsive symbiotic relationship between them and their Behemoth, no longer a network of varying abilities. 
Mutation was now also not even possible. Instead, mutation was catalyzed by absorbing Naethryn Denoth into themselves. 
Mutations then had benefits and connections to the other Realm, and the power and ability the denoth exhibited. Mutation results became unpredictable and varied in many ways. Class names became useless reclis help by the elder survivors, the origional [Hul'garrei](/artefact/the-hulgarrei) and some [Abusk'hep](/artefact/the-abuskhep)

# Physilogy

## Feet and hands

Some Auja develop hand-like feet that allow them to walk on a variety of surfaces, including walls and ceilings. Gecko-like claws that either dig in and grip or suction on. With the articulation of fingers. Prehensile
It is needed as while the Behemoth is truly massive, his movements sway and shift and shake those on his exterior. Much like sea legs, they are used to the movement and can grip where they are standing.

Hands and fingers are used for glyphing, fingers wedged for carving and gouging into the Behemoth. Wide palms that have different types of saliva glandas in the folds, used to mix with different substances to create different magical inks to glyph with.
Individuals have signiature saliva particular to them, making it possible to tell apart glyphs as well as leaving a kind of time stamp and personal style and message along with it.

## Features

Flat forehead and nose, angular planed features. Slanted, lidded eyes, shallow sockets and unpronounced eyebrow ridge. 
Post mutation, arms may lengthen and forearms and hands widen, as they are used for everything.
Their skin is tough and hardened into plated sections in some areas, more flexible yet leathery skin around their joints. They look as if made from clay, sculpted, cracked and dried. Complexion like sun-baked terracota.


# Naming

First name is an Auja's given name upon birth, what their family and brood mates would call and refer to them.

Second name is divided into the name their brood was given, and the honorific of their selected class.

Third name is that of their parent, usually their mother's. In the case of two parents, they have both.

— eg: *[Aahepthes gerz'hul Taheres](/character/aahepthes)* - Given name *Aahepthes*, brood *gerz*, class *[hul'garrei](/artefact/the-hulgarrei)*, mother *Taheres*.

— eg:  *[Aamenemhet yarz'hep Yarra](/character/amenemhet)* - Given name *Aamenemhet*, from brood *yarz*, class *[abusk'hep](/artefact/the-abuskhep)*, mother *Yarra*.



## Exceptions

With no class mutation their title is 'tep, essentially denied or rejected. This is very unusual and somewhat of an insult, and makes it clear they have no place in the Auja. Those who were unable to mutate with the behemoth, and instead stayed in their non gender state, were usually given this title. Some who were unwilling to take on their mutations responsibilities to both Ogtbish and the Aujauran would have their title replaced and then removed from 

Those who are born individually and without a brood are given the name Kilau, which means alone or single, essentially no brood family. 

Example:

[Tejheira kilau'tep](/character/tejheira), who is also special because she was not given her mothers name, or her fathers. She was given no ties to the Auja in her name on purpose, emphasising her not belonging.



# Food and sustenance

---

## Hunting techniques

---

## Food gathering rituals

---

# Social Rituals

