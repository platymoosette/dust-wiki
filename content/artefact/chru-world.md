---
date: 2019-02-06T13:13:04+02:00
description: "the world of Dust and Behemoths"
draft: false
featured: false
image: ""
share: false
slug: chru-world
tags:
- Artefact
- Varsin
- Story
title: Chru
---

Var'sin is changing. Everyone can feel it. There are more cases of dust storms, mutations, rogue reality whisps that alter what they touch. More Alter beasts and beings are tearing through into their plane. Recovered and repurposed technology is "malfunctioning", or reawakened when before dead. Other things have been amplified. Opening gaps into the other Planes is becoming more unstable and dangerous.

The ones who still believe in Gods are noticing a change. Areas beset by Gods for generations have them wander off, just disappear. Or in some worse cases, they become mad. Distorted and blinded by a sudden implosion of inhumanity (?), a portal to alien, alter planar vortexes of power in a messy physical puppet in this realm.

Things are shifting in Chru.

The Gods, left overs from when the world first existed, mistakes and broken forms still holding on to existence through the aeons. Shreds and broken connections held together with a vision of the past, merely drifting around like gosts, knowing they have no real need to intersect with this new existence. They still act as if the world is the same. But it's mutated and destroyed. Twisted and altered. Stripped of all that was. Now inhabited by generations of different beings, sordid shadows of who they once were. Accompanied by other races, evolved from others that both crossed over and mutated from Dust and Naethryn.

Some can sense a massive force, almost magnetic. A wrongness, differentness, seeping through the fabric of their plane. They need to find it. They gravitate towards it. Intent on ripping through reality on their way across the world. Along with their wanton destruction on their sudden migration, the world is clouded and polluted by the Dust. Existing for so long they (the left overs) have no idea of a life without it. It is destructive, decaying, unstable and extremely powerful. In clusters it coalesces into a jagged connection through to other Planes, expelling forth aether whisps and beings of unreality. Things that twist the very nature of life. Mutating. Decaying.

Decaying..

