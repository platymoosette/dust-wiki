---
date: 2019-02-07T13:53:48+02:00
description: ""
draft: false
featured: false
image: ""
share: false
slug: plane-travelling
tags:
- Artefact
- Planes
- Rifts
- Travelling
title: Plane Travelling
---

You enter into the other plane by opening a rift into/through connections. The skill / talent / ability is in choosing where to travel, through what plane, depending on where you want to be or where you want to go.

There are maps, learned connections that others have used before. Others just have an intution, an ability to explore. Hunters that know exactly where certain beings or aethers congregate or exist. Knowing where there are other beings means when the hunter opens up the rift, they will be able to reach through and pull an naethyr being through.

Manipulating after brought through is another skill. Pulling them into objects. Pulling them into similiar forms as their true one, but constricted and controlled. Or brought into other bodies, creatures or people (posession). Or summoned into the Rifter, imbueing them with power, with spells, with knowledge of other planes and worlds, able to commune with other beings, naethryns, aethryns.



[Aujauran Se'ra](/artefact/the-auja) - Use rifting and travelling. 