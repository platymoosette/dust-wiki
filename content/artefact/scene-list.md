---
date: 2016-11-19T19:45:19+02:00
description: "List of scenes for Tej and the Behemoth"
draft: false
featured: false
image: ""
share: false
slug: scene-list
tags:
- Artefact
- Scenes
- Aujauran Se'ra
- Ogtbish
title: Scene List

---

##Tej

- Tej watching Amen’s hands signal glyphs and noticing the differences. [link](/artefact/scene-hands)
- Tej encounters a child from another brood and is verbally bullied. Though younger they are taller, larger, and are starting to work glyphs, putting them on another level to her that she will never be allowed to reach. [link](/artefact/scene-bully)
- Nearly bumping into Amen while wandering through tunnels. Tej decides to follow and is led on a winding but ever upwards climb, abruptly ending. Amen shuffles through her bags and places something in a recessed part of the tunnel wall, covers it, takes a moment to stare up the tunnel. Tej is forced to find a place to hide while Amen turns and makes her way back.
- Tej ventures forward to investigate what Amen left behind, and discovers it is a  mostly food and water, and a few oddly shaped beads in a soft wethyr bag that was vile to the touch, she could barely handle it without feeling sick. Pondering, she goes back to hiding, and waits for recipient of the package…
- But she waits too long, and no one comes, so she goes back late. Is met by an angry Amen.

- Finds the remnants of an Auja she doesn't know. Rooms and some of their summoning and glyphs that she traces,  a sense of something larger...
- Entranced by the dust, she ends up humming? Amen *knows*, senses? worries?



## Calos

- Guiding people through the planes
- Attacked/accident, stranded and wounded with his tech broken.
- Somehow finds ruins and takes refuge.
- Taken in by Auja. Hes symbolic of a world they want again.



## Aujauran

- Aahep and Naminah were exploring the Naethyr, making contact with more Naethryn, more bubbles of unreality and the twisted beings in between.
- They were the first few to be summoned into as rite of passage? Continue the ritual. Making t the only useful Class to be in