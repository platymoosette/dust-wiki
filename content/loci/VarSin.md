---
date: 2016-10-01T19:38:26+02:00
description: "The vertical city."
draft: false
featured: true
image: ""
share: false
slug: varsin-city
tags:
- Loci
- Varsin
- Nuru
title: Var'sin City
---

# Discovery and a new life

They were just a sun branded bunch of nomads. The Behemoth of their land had gone mad, and started torturing and destroying them. Their home uninhabitable, they had to move. They travelled through dust ravaged planes, sun struck patches hot enough to boil, vast caverns and mountains of reality distorted land. It was wild and harrowing, but they managed to prosper for some bizarre reason. The taint of their [satoh](/artefact/terminology) had warped them over time, along with the dust exposure. 
Chased, yearning for home, they stumble across the hollowed canyon that let down its barriers on arrival. That led them to a safe haven made of old, decaying buildings and structures, slowly crystaline reawakening.
Able to survive and thrive in a secure and empty refuge, they recover old tech they find and gather. Trying to piece together the clues of the previous inhabitants.
But they’re still piratic, nomadic, dust stained as they’d always been. Some have turned to trying to turn on as much of the City as possible. The others use the protective gear and items found in supplies to help them survive the ravages of the Dust planes alone and scavenge for old Tak. Which they dont always figure out. And when they do, they often malfunction… Something about the new world made them defunct. On the other hand, the City seemed to be awakening itself, with their presence and their harvested dust. It spread through the City like a drop of ink, bringing life to the past. A slow roiling of distorting power.
They were still mad. They thought they lived in the presence of ancient spirits that awoke, or a Behemoth being summoned.
 They were not far wrong.

The very nature of Dust is the reason the awakening and discovery of the blanketed City was so important to the world, and one of the only reasons so many of the worlds life has survived.
The city was found by those that had vaguely similar DNA as its creators, allowing the initial breach into an entirely isolated and preserved City spanning across a massive cliff face and mountainous valley. 
Old Tech kept it protected from the tempest that destroyed the Old ones.



# Sooo.. this City?



The City is a shining, grasping Pinnacle of true power and ascension and the wonders of Mana tech, mired and held down by the spreading mass of the lower sectors.
The Taken are some of the most powerful Magi, the most adept at both mana use and Planar summoning. There are “lower” ranks (though still formidable) who deal with forming portals and summons and channeling mana to infuse themselves. The real Taken are much further along in their experience and mutation….
Drawn in by the other Plane (Naethyr)  and the beings and naethryns/aethryns that inhabit it, they have gone further and deeper into a force they have no idea about the depth or nature of. Summoned into in order to be blasted, infused and connected to the other Plane while overriding the ethers self with pure power and the surge of mana. Many are burned or iced out by the pure shock and overwhelming force and presence, either dying or turned vegetable, or even worse, just unable to feel /anything/. The best and most proficient soon become so infused and boiling with not only the other Planar power and mana, but the powerful summons they need to keep the connection.  They are almost entirely changed. Their goals and sights are set on a terrifyingly focused blend of human and Naethyr.
They strive not for human gain or benefit, but the exploration of Mana and Planes, morphed and mutated into half Naethyrn. These Taken are not ever seen or dealt with, yet they are the force and Core of the City (Vars’in). Without them no mana or improvement or opening of sectors.
But they know not of what they use or awaken… The sudden surges and leaking of the Other Plane is seeping into other places, awakening and feeding possibilities that would not have been possible before. Mutations are occurring in creatures, entirely new beings entering insidiously through the leaks, strange distorted mana drifts around and touches minds that were previously unfocused.. Their reckless yet restrictive gasp of Mana hurts large parts of the lower sectors too. Mana tech is mostly restricted to their Sector, and mana only dabbled in the Mana Tech Sector (MTS) , a focused and pure Mana Sector sharing some boundaries with them, but they are very much different. 
No respect is given to those below them in the Lower Sectors, let alone tolerance. Snubbed, treated badly by Taken Guardians and even Cursed. Which the Taken fling around contemptuously, cutting the Cursed’s connection to mana, effectively destroying their lives. And eventually even harvesting them for the Chamber…