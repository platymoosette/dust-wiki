---
description: ""
draft: false
featured: false
image: ""
share: false
slug: character-name
tags:
- Character
title: Character Name
---

# General info

Race:

Gender:

Other names:

Birth Place:

Relations: 

Abilities:

Appearance:



## Bio



---



## Personal Attributes

**Primary Motivator**: 

**Emotional Disposition:** 

**Moodiness:**

**Outlook**: 

**Integrity**: 

**Impulsiveness**: 

**Boldness:** 

**Agreeableness**:  

**Interactivity**: 

**Conformity:** 

### Personality

**Humour:**  

**Topics of convo**: 

**Adherence**:

**Tolerance of others**:

**Quirks and habits**:

**Religion**: 

**Hobbies:**  

---

**Surface Affectations**:

**Physical affectations**:

**Personality**:

**Inner demons and conflicts:**

**Worldview:**

**Goals and motivations:**

**Anima:**

(How does your character act when they are really being their self?)

**Persona:**

(How does your character act to hide their real self?)

**Decisions, actions and behaviours**:

####Character Arc

---



# Links

